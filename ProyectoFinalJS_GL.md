﻿**Proyecto Final - Curso de introducción a JavaScript**
*García Rodríguez Luis Armando*

Para resolver la practica final de curso, tome la libertad de que en lugar de agregar elementos a una lista, se agregaran div sobre un section con el id #peliculas, y que pudiera mostrar la imagen, el titulo y la descripción de la pelicula. Para esto, me enfoque en entender el funcionamiento de index.js para posteriormente replicar o emular el funcionamiento de este código pero para buscar.js y específicamente de la función crearMovieCard.

Para que funcionara correctamente sustituir en el HTML el div id #resultados, por :

       <section id="peliculas" class="row mt-4">
    
       </section>


Estas son las líneas claves en la  integración  del código para agregar divs sobre el section con los resultados de la consulta a la API:

     var miLista = `
                            <div class="col-md-4 mb-3">
                                <div class="card">
                                    <img src="https://image.tmdb.org/t/p/w500${pelicula.poster_path}" class="card-img-top" alt="${pelicula.title}">
                                    <div class="card-body">
                                        <h5 class="card-title">${pelicula.title}</h5>
                                        <p class="card-text">${pelicula.release_date}</p>
                                        <p class="card-text">${pelicula.overview}</p>
                                    </div>
                                </div>
                            </div>
                        `;
                        $("#peliculas").append(miLista);
                    });

También hice el código que se pidió en el ejercicio que esta en el archivo buscar.js, sin embargo el archivo funcionando es el buscar2.js

Quise agregar imágenes del resultado pero no me deja :(
Espero no tengan problemas para visualizarlo :)
