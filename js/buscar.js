$(document).ready(function(){
    $("#btn-buscar").on("click", function(){
        const palabra = $('#busqueda').val();
        if (palabra) {
            $.ajax({
                url: "https://api.themoviedb.org/3/search/movie?api_key=3356865d41894a2fa9bfa84b2b5f59bb&query=" + palabra,
                success: function(respuesta) {
                    $("#resultados").empty();
                    var miLista = $("#miLista")

                    respuesta.results.forEach(pelicula => {
                        miLista.append(`<li>${pelicula.title} - ${pelicula.release_date}</li>`);
                    });
                },
                error: function() {
                    alert("Error al buscar películas");
                }
            });
        } else {
            alert("No buscaste nada, da enter e introduce algo");
        }
    });
});


