$(document).ready(function(){
    $("#btn-buscar").on("click", function(){
        const palabra = $('#busqueda').val();
        if (palabra) {
            $.ajax({
                url: "https://api.themoviedb.org/3/search/movie?api_key=3356865d41894a2fa9bfa84b2b5f59bb&query=" + palabra,
                success: function(respuesta) {
                    $("#peliculas").empty();
                    respuesta.results.forEach(pelicula => {
                        var miLista = `
                            <div class="col-md-4 mb-3">
                                <div class="card">
                                    <img src="https://image.tmdb.org/t/p/w500${pelicula.poster_path}" class="card-img-top" alt="${pelicula.title}">
                                    <div class="card-body">
                                        <h5 class="card-title">${pelicula.title}</h5>
                                        <p class="card-text">${pelicula.release_date}</p>
                                        <p class="card-text">${pelicula.overview}</p>
                                    </div>
                                </div>
                            </div>
                        `;
                        $("#peliculas").append(miLista);
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error al buscar películas: " + textStatus + ", " + errorThrown);
                }
            });
        } else {
            alert("Por favor ingrese una palabra para buscar");
        }
    });
});